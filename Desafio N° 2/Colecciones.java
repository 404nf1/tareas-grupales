import java.util.*;

public class Colecciones {
    private ArrayList<Salas> salas = new ArrayList<>();
    private ArrayList<Empleados> empleados = new ArrayList<>();
    private ArrayList<Acomodadores> acomodadores = new ArrayList<>();
    private ListIterator<Salas> RecorrerSalas;
    private ListIterator<Empleados> buscarEmpleado;
    private ListIterator<Acomodadores> BuscarAcomodador;
    private Scanner sc = new Scanner(System.in);

    public void EditarOAgregarEmpleado(String nombre, int edad) {
        boolean Empleado = false;
        boolean Acomodador = false;

        buscarEmpleado = empleados.listIterator();
        while (buscarEmpleado.hasNext())
        {
            if (buscarEmpleado.next().getNombre().equals(nombre))
            {
                buscarEmpleado.previous();
                if (buscarEmpleado.next().getEdad() == edad)
                {
                    Empleado = true;
                    break;
                }
            }
        }

        BuscarAcomodador = acomodadores.listIterator();
        if (!Empleado)
        {
            while (BuscarAcomodador.hasNext())
            {
                if (BuscarAcomodador.next().getNombre().equals(nombre))
                {
                    BuscarAcomodador.previous();
                    if (BuscarAcomodador.next().getEdad() == edad)
                    {
                        Acomodador = true;
                        break;
                    }
                }
            }
        }

        if (Empleado || Acomodador)
        {
            System.out.println("¿Desea modificar el salario del empleado?(s/n)");
            char si_no;

            do {
                si_no = sc.next().charAt(0);
            } while (si_no != 's' && si_no != 'n');

            if (si_no == 's')
            {
                System.out.println("Ingres salario del empleado");
                double SalarioEmpleado;

                do {
                    SalarioEmpleado = try_catch(sc.next());
                    if (SalarioEmpleado > -1 && SalarioEmpleado < 17500)
                    {
                        System.out.println("El salario está por debajo del mínimo");
                    }
                } while (SalarioEmpleado < 17500);

                if (Empleado)
                {
                    buscarEmpleado.previous();
                    buscarEmpleado.next().setSueldo(SalarioEmpleado);
                }
                else
                {
                    BuscarAcomodador.previous();
                    BuscarAcomodador.next().setSueldo(SalarioEmpleado);
                }
            }

            if (Acomodador)
            {
                System.out.println("¿Desea modificar la sala asignada?(s/n)");

                do {
                    si_no = sc.next().charAt(0);
                } while (si_no != 's' && si_no != 'n');

                if (si_no == 's')
                {
                    boolean existe = false;

                    if (salas.size() != 0) {
                        System.out.println("Ingrese nombre de la sala");
                        do {
                            String NombreSala = sc.next();
                            for (Salas sala : salas) {
                                if (sala.getNombre().equals(NombreSala)) {
                                    existe = true;
                                    BuscarAcomodador.previous();
                                    BuscarAcomodador.next().setSala(sala);
                                    break;
                                }
                            }

                            if (!existe) System.out.println("No existe la sala");
                        } while (!existe);
                    }
                    else System.out.println("No hay salas");
                }
            }
        }
        else
        {
            System.out.println("No existe el empleado, ¿Desea añadir un empleado?(s/n)");
            char si_no;

            do {
                si_no = sc.next().charAt(0);
            } while (si_no != 's' && si_no != 'n');

            if (si_no == 's')
            {
                System.out.println("¿El empleado es un acomodador?(s/n)");

                do {
                    si_no = sc.next().charAt(0);
                } while (si_no != 's' && si_no != 'n');

                if (si_no == 'n')
                {
                    Empleados empleado = new Empleados(nombre, edad);
                    double SalarioEmpleado = Salario();
                    empleado.setSueldo(SalarioEmpleado);
                    empleados.add(empleado);
                    empleados.sort(Comparator.comparing(Empleados::getNombre));
                }
                else
                {
                    Acomodadores acomodador = new Acomodadores(nombre, edad);
                    double SalarioEmpleado = Salario();
                    acomodador.setSueldo(SalarioEmpleado);

                    System.out.println("Asígnele una sala");
                    String NombreSala;
                    boolean existe = false;

                    do {
                        NombreSala = sc.next();
                        RecorrerSalas = salas.listIterator();
                        while (RecorrerSalas.hasNext()) {
                            if (RecorrerSalas.next().getNombre().equals(NombreSala)) {
                                RecorrerSalas.previous();
                                acomodador.setSala(RecorrerSalas.next());
                                existe = true;
                                break;
                            }
                        }
                        if (!existe) System.out.println("La sala no existe");
                    } while (!existe);

                    acomodadores.add(acomodador);
                    acomodadores.sort(Comparator.comparing(Acomodadores::getNombre));
                }
            }
        }
    }

    public boolean EliminarEmpleado(String nombre, int edad)
    {
        boolean eliminado = false;

        buscarEmpleado = empleados.listIterator();
        while (buscarEmpleado.hasNext())
        {
            if (buscarEmpleado.next().getNombre().equals(nombre))
            {
                buscarEmpleado.previous();
                if (buscarEmpleado.next().getEdad() == edad)
                {
                    buscarEmpleado.remove();
                    eliminado = true;
                }
            }
        }

        if (!eliminado) {
            BuscarAcomodador = acomodadores.listIterator();
            while (BuscarAcomodador.hasNext()) {
                if (BuscarAcomodador.next().getNombre().equals(nombre)) {
                    BuscarAcomodador.previous();
                    if (BuscarAcomodador.next().getEdad() == edad) {
                        BuscarAcomodador.remove();
                        eliminado = true;
                    }
                }
            }
        }

        return  eliminado;
    }

    public void addSala(Salas sala) {
        this.salas.add(sala);
        salas.sort(Comparator.comparing(Salas::getNombre));
    }

    public ArrayList<Salas> getSalas() {
        return salas;
    }

    public ArrayList<Empleados> getEmpleados() {
        return empleados;
    }

    public ArrayList<Acomodadores> getAcomodadores() {
        return acomodadores;
    }

    public boolean EliminarSala(String nombre)
    {
        RecorrerSalas = salas.listIterator();
        while (RecorrerSalas.hasNext())
        {
            if (RecorrerSalas.next().getNombre().equals(nombre)) {
                BuscarAcomodador = acomodadores.listIterator();
                while (BuscarAcomodador.hasNext())
                {
                    if (BuscarAcomodador.next().getSala().getNombre().equals(nombre))
                    {
                        BuscarAcomodador.previous();
                        BuscarAcomodador.next().setSala(null);
                    }
                }

                RecorrerSalas.remove();
                return true;
            }
        }

        return false;
    }

    public void AsignarEspectadoresSala(String NombreSala, LinkedList<Espectadores> espectadores)
    {
        espectadores.sort(Comparator.comparing(Espectadores::getNombre));
        RecorrerSalas = salas.listIterator();
        while (RecorrerSalas.hasNext())
        {
            if (RecorrerSalas.next().getNombre().equals(NombreSala))
            {
                RecorrerSalas.previous();
                RecorrerSalas.next().setEspectadores(espectadores);
            }
        }
    }

    @Override
    public String toString() {
        return "Colecciones{" +
                "salas=" + salas +
                ", empleados=" + empleados +
                ", acomodadores=" + acomodadores +
                '}';
    }

    private static double try_catch(String dato) {
        double x;
        try {
            x = Double.parseDouble(dato);
        } catch (Exception e) {
            System.out.println("ERROR EN EL INGRESO DE DATOS");
            return -1;
        }
        return x;
    }

    private double Salario() {
        double SalarioEmpleado;
        System.out.println("Ingres salario del empleado");

        do {
            SalarioEmpleado = try_catch(sc.next());
            if (SalarioEmpleado > -1 && SalarioEmpleado < 17500)
            {
                System.out.println("El salario está por debajo del mínimo");
            }
        } while (SalarioEmpleado < 17500);

        return SalarioEmpleado;
    }
}
